package org.example.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CountResponseDto {
    private Long id;
    private Integer count;
}

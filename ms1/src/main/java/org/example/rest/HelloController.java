package org.example.rest;

import lombok.RequiredArgsConstructor;
import org.example.dto.CountResponseDto;
import org.example.service.CountService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/ms1")
@RequiredArgsConstructor

public class HelloController {
    private final CountService countService;

    @GetMapping("/{id}")
    public CountResponseDto getCount(@PathVariable Long id) {
        return countService.getCountById(id);
    }
}

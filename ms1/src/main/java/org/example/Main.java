package org.example;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import lombok.RequiredArgsConstructor;
import org.example.domain.StudentEntity;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@RequiredArgsConstructor
@SpringBootApplication
public class Main  implements CommandLineRunner {

    private final EntityManagerFactory entityManagerFactory;
    StudentEntity studentEntity =
            StudentEntity
                    .builder()
//                        .id(1L)
                    .name("Badambura")
                    .surname("Badamov")
                    .age(77)
                    .address("Kamcatka")
                    .city("6783975rhfndfg")
                    .gender("male")
                    .build();

    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
    }

    @Override
//    @Transactional
    public void run(String... args) throws Exception {

        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();


        entityManager.persist(studentEntity);
        System.out.println("Managed " + entityManager.contains(studentEntity));

        entityManager.detach(studentEntity);
        System.out.println("Detached " + entityManager.contains(studentEntity));

       final StudentEntity studentEntityMerge =  entityManager.merge(studentEntity);
        System.out.println("Managed " + entityManager.contains(studentEntityMerge));

        entityManager.clear();
        System.out.println("Detached " + entityManager.contains(studentEntity));

        entityManager.find(StudentEntity.class, studentEntity.getId());
        System.out.println("Managed " + entityManager.contains(studentEntity));

        transaction.commit();
        entityManager.close();
//        System.out.println(entityManager.contains(studentEntity));


    }
}

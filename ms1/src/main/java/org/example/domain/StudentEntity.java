package org.example.domain;

import jakarta.persistence.*;
import lombok.*;

import java.awt.print.Book;
import java.util.List;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@ToString(exclude = {"courseEntity","bookEntity","studentDetailEntity"})
public class StudentEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String surname;
    private Integer age;
    private String address;
    private String city;
    private String gender;
    @OneToOne(cascade = CascadeType.PERSIST , mappedBy = "studentEntity", fetch = FetchType.LAZY)
    private StudentDetailEntity studentDetailEntity;
    @OneToMany(mappedBy = "studentEntity" , cascade = CascadeType.PERSIST,fetch = FetchType.LAZY)
    private List<BookEntity> bookEntity;
    @ManyToMany(cascade = CascadeType.PERSIST,fetch = FetchType.LAZY)
    private List<CourseEntity> courseEntity;
}

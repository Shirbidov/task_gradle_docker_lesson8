package org.example.domain;

import jakarta.persistence.*;
import lombok.*;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@ToString(exclude = "studentEntity")
@Builder
@Data
public class StudentDetailEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String pin;
    private String picture;
    @OneToOne(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    private StudentEntity studentEntity;
}

package org.example.repository;

import org.example.domain.StudentDetailEntity;
import org.example.domain.StudentEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentDetailRepository extends JpaRepository<StudentDetailEntity, Long> {
}

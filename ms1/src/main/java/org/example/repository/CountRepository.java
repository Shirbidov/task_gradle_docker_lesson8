package org.example.repository;

import org.example.domain.CountEntity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

public interface CountRepository extends JpaRepository<CountEntity, Long> {

    CountEntity findCountById(Long id);
//    @Transactional
//    @Modifying
//    @Query(value = "UPDATE count_entity set count = count + 1 WHERE id =:id;" , nativeQuery = true)
//    void counting();

}

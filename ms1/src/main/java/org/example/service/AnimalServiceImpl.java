package org.example.service;

import org.springframework.stereotype.Component;

@Component
public class AnimalServiceImpl implements AnimalService {

    @Override
    @LoginExecution
    public void makeNoise() {
        System.out.println("Bark");

    }
}

package org.example.service;

import lombok.RequiredArgsConstructor;
import org.example.domain.CountEntity;
import org.example.dto.CountRequestDto;
import org.example.dto.CountResponseDto;
import org.example.repository.CountRepository;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CountImpl implements CountService {
    final CountRepository countRepository;

    @Override
    public CountResponseDto getCountById(Long id) {
        final CountEntity countEntityGet = countRepository.findCountById(id);
//        countEntityGet.setCount(countEntityGet.getCount());
//        countRepository.save(countEntityGet);
        return CountResponseDto
                .builder()
                .id(countEntityGet.getId())
                .count(countEntityGet.getCount())
                .build();
    }
    /*
    public CountResponseDto getCountById(Long id) {
    CountEntity countEntity = countRepository.findCountById(id);

    // Increment the count
    countEntity.setCount(countEntity.getCount() + 1);
    countRepository.save(countEntity); // Save the updated count

    return CountResponseDto.builder()
            .id(countEntity.getId())
            .count(countEntity.getCount())
            .build();
}
     */

//    @Override
//    public CountResponseDto updateCount(Long id, CountRequestDto countRequestDto) {
//        CountEntity countEntity =
//                CountEntity
//                        .builder()
//                        .id(id)
//                        .count(countRequestDto.getCount()+1)
//                        .build();
//        final CountEntity countSaved = countRepository.save(countEntity);
//        return CountResponseDto
//                .builder()
//                .id(countSaved.getId())
//                .count(countSaved.getCount())
//                .build();
//    }

/*
    public DeviceResponseDTO updateDevice(Long id, DeviceRequestDTO requestDTO) {
        DeviceEntity device =
                DeviceEntity.
                        builder()
                        .id(id)
                        .name(requestDTO.getName())
                        .serialNumber(requestDTO.getSerialNumber())
                        .build();
        final DeviceEntity deviceEntitySaved = deviceRepository.save(device);
        return DeviceResponseDTO
                .builder()
                .id(deviceEntitySaved.getId())
                .name(deviceEntitySaved.getName())
                .serialNumber(deviceEntitySaved.getSerialNumber())
                .build();
    }
 */



}

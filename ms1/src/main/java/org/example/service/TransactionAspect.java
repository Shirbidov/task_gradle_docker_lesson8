package org.example.service;

import lombok.SneakyThrows;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class TransactionAspect {

    @SneakyThrows
    @Around("@annotation(org.example.service.LoginExecution)")
    public void methodBeforeAdvice(ProceedingJoinPoint proceedingJoinPoint){
        System.out.println("Animal will back , now !");
        proceedingJoinPoint.proceed();
        System.out.println("Animal stopped barking");
    }
}

package org.example.service;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.example.domain.StudentEntity;
import org.example.repository.StudentRepository;
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService{

private final StudentRepository repository ;

    @Override
    @Transactional
    public void testTransaction() {
        final StudentEntity studentEntity = repository.findById(1L)
                .orElseThrow();

        studentEntity.setName("Kamaladdin");
        studentEntity.setAge(30);
    }
}

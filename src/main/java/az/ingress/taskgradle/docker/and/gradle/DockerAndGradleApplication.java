package az.ingress.taskgradle.docker.and.gradle;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DockerAndGradleApplication {

	public static void main(String[] args) {
		SpringApplication.run(DockerAndGradleApplication.class, args);
	}

}

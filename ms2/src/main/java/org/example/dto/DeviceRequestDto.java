package org.example.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DeviceRequestDto {

    //todo notBlank
    private String manufacture;
    private String model;
    private Long count;
}

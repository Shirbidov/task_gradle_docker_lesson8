package org.example.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DeviceResponseDto {

    private Long id;
    private String manufacture;
    private String model;
    private Long count;

}

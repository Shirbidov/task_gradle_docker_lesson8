package org.example.service;

import lombok.RequiredArgsConstructor;
import org.example.domain.Device;
import org.example.dto.DeviceRequestDto;
import org.example.dto.DeviceResponseDto;
import org.example.repository.DeviceRepository;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class DeviceImplement implements DeviceService {

    final DeviceRepository deviceRepository;


    @Override
    public DeviceResponseDto countIncrement(DeviceRequestDto deviceRequestDto) {
        Device device =
                Device.
                        builder()
                        .manufacture(deviceRequestDto.getManufacture())
                        .model(deviceRequestDto.getModel())
                        .count(deviceRequestDto.getCount())
                        .build();
        Device deviceSaved = deviceRepository.save(device);
        return DeviceResponseDto
                .builder()
                .id(deviceSaved.getId())
                .manufacture(deviceSaved.getManufacture())
                .model(deviceSaved.getModel())
                .count(deviceSaved.getCount())
                .build();
    }
}

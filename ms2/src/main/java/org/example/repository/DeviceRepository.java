package org.example.repository;

import org.example.domain.Device;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DeviceRepository extends  JpaRepository<Device,Long> {



}
